TD3 JavaScript − jQuery / Validation de formulaire
==================================================

Nous allons pour cet exercice utiliser JavaScript et jQuery pour
*prévalider* des champs de formulaire (c'est à dire les valider avant
même que l'utilisateur n'envoie le formulaire).

Pour cet exercice, **nous n'enverrons jamais réellement le formulaire** à un
serveur. Si c'était le cas, les mêmes validations devraient **être codées
une seconde fois côté serveur**.

Exo 1. Formulaire
-----------------

*(cet exo ne nécessite ni jQuery, ni JavaScript, simplement du HTML)*

Créer un formulaire HTML avec les champs suivants :

-   Nom (requis)
-   Prénom (requis)
-   date de naissance (requis)
-   Numéro de téléphone
-   Adresse (faites simple : un seul champ pour adresse, ville… etc)

+  un bouton d'envoi.

Exo 2. Nom/Prénom
------------------

*(cet exo ne nécessite ni jQuery, ni JavaScript, simplement du HTML puis du CSS)*

-   Utiliser les bons attributs HTML5 du champ `<input>` pour faire une
    prévalidation des champs requis (vérifier qu'ils sont bien remplis). cf
    [doc abordant le sujet](http://webeur.blogspot.fr/2013/12/formulaires-html5-required-autofocus.html)
-   Ajouter en CSS un fond vert (vert clair hein, préservez vos yeux…) aux
    champ de formulaires *valides* (piste: il faut utiliser la pseudo-classe
    CSS dédiée à cet usage)

Exo 3. Prévalidation
---------------------

*(cet exercice met en œuvre JavaScript et jQuery)*

Nous allons ajouter via JavaScript des règles de prévalidation, et de petites
aides à la saisie.

### 3.1 Date de naissance

#### Préambule

Assurez-vous que vous utilisez bien un `<input type="date" />` pour la date de
naissance. Testez sur différents navigateurs (Firefox et Chrome). Que constatez
vous ?

*Pour la suite de cette question nous allons utiliser un `<input type="text" />`
pour la date de naissance, afin d'avoir un résultat homogène dans tous les
navigateurs.*

#### Le vif du sujet…

Nous allons utiliser jQueryUI, qui est un très gros plugin jQuery incluant de
nombreuses fonctionalités.

Nous utiliserons ici uniquement sa fonctionalité de *datepicker* (cf [doc de
cette fonctonalité](http://jqueryui.com/datepicker/)).

Utiliser la fonctionalité **datepicker** du plugin *jQueryUI* *pickADate* pour
afficher un mini-calendrier servant à sélectionner une date.

Télécharger et décompresser le plugin quelque-part dans le dossier de votre TD :
<http://jqueryui.com/resources/download/jquery-ui-1.12.1.zip>

L'inclure dans votre page ; la documentation officielle conseillant une autre
méthode plus complexe (*custom build*) voici une aide pour mettre en œuvre le zip
fourni :

Il vous faut lier à votre document HTML différents fichiers contenus dans le
fichier zip que vous avez décompressé (à vous de retrouver les chemins exacts):

- via une balise `<script>` le fichier `jquery-ui.js`
- via une balise `<link>` le fichier `jquery-ui.css`

Par défaut la date est au format **jj/mm/AAAA**. Paramétrer le plugin pour que
la date produite soit au format **jj-mm-AAAA** (ex: **12-10-1999**).

Les options disponibles sont documentées ici : <http://api.jqueryui.com/datepicker/>.

### 3.2 Numéro de téléphone

*(cette question 3.2 est consituée purement de HTML, pas de JavaScript)*

Nous voulons accepter uniquement les formats de numéro de téléphone
français valides.

Nous allons pour cela utiliser une *regexp*, c'est à dire un « patron »
permettant de valider le format d'une chaine de caractères.

Pour cela, utiliser l'attribut du champ (voir [doc abordant le
sujet](http://webeur.blogspot.fr/2013/12/formulaires-html5-required-autofocus.html))
qui permet de faire valider un champ par un regexp.

Vu que nous n'avons pas abordé le fonctionnement des *regexp*, vous
pouvez récupérer une regexp toute prête pour les numéros de téléphone
français depuis [ce site](http://html5pattern.com).

### 3.3 Adresse (BONUS)

Nous allons utiliser la base adresse nationale : <https://adresse.data.gouv.fr/api/>

- Suggérer une liste d'adresses (dans un `<ul>`) au fur et à mesure que les utilisateurs
tapent la requête (afficher le champ du JSON retourné) ;

- Un clic sur une des adresses proposées doit remplir le champ d'adresses avec
  sa valeur (et cacher le `<ul>` de suggestions).

### 3.4 Carte (BONUS)

Modifier le traitement précédent :

- Ajouter au code HTML deux `<input type=hidden>` (pour l'instant vides) pour
  contenir la latitude et la longitude de l'adresse sélectionnée.
- Modifier le traitement de la question précédent pour, au clic sur
  une adresse, remplir, en plus du champ adresse, ces deux champs cachés.
- Afficher l'adresse choisie sur une mini-carte à l'aide de leaflet.

cf [intro à leaflet](https://zestedesavoir.com/tutoriels/1365/des-cartes-sur-votre-site/)

### 3.5 Tan (BONUS)

*Vous devez désactiver la protections CORS pour cette question*

Rechercher l'arrêt de transport le plus proche de l'adresse et l'afficher à
l'utilisateur.

La recherche se fait (sur l'API de la TAN) en précisant latitude et
longitude, par exemple :

<http://open_preprod.tan.fr/ewp/arrets.json?latitude=47.213443&longitude=-1.557983>

cf aussi
[la documentation de l'API TAN](https://data.nantes.fr/fileadmin/nm_opendata/pdf/Semitan-Documentation-API-V3.pdf).
